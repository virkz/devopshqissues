FROM python:3

RUN pip install --upgrade pip && \
    pip install requests

ADD github-cli.py /

ENTRYPOINT [ "python", "./github-cli.py" ] 
