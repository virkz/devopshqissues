import requests

print('Список репозиториев в https://github.com/devopshq:')

user = requests.get('https://api.github.com/users/devopshq/repos').json()
list_repositories = []

for repo in range(len(user)):
    full_name = user[repo]['full_name']
    list_repositories.append(full_name)
    print(f'{repo}.', full_name)

while True:
    try:
        select_repository = int(input('Введите номер из списка репозиториев для просмотра заданий: '))
    except ValueError as e:
        print('Неверный формат')
    if 0 <= select_repository < len(list_repositories):
        break
    else:
        print(f'Неверное значение {select_repository}')

repository = list_repositories[select_repository]
issues = requests.get('https://api.github.com/repos/' + repository + '/issues?state=all').json()

for issue in issues:
    issue_id = issue['number']
    title = issue['title']
    print(f'{repository} \n#{issue_id} \n{title}\n\n')

input('Press any key to exit')
