import pytest
import requests

@pytest.fixture
def len_user_repos():
    user_json = requests.get('https://api.github.com/users/devopshq/repos').json()
    len_repos = len(user_json)
    return len_repos

def test_input(len_user_repos):
    for i in range(len_user_repos):
        assert 0 <= i < len_user_repos
