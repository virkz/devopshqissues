import requests
import pytest

@pytest.fixture
def get_issues():
    issues_json = requests.get('https://api.github.com/repos/devopshq/artifactory/issues?state=all').json()
    for issue in issues_json:
        issue_id = issue['number']
        title = issue['title']
    return issue_id, title

def test_type(get_issues):
    assert isinstance(get_issues, tuple)