import requests
import pytest

@pytest.fixture
def read_repos():
    reps = []
    user_json = requests.get('https://api.github.com/users/devopshq/repos').json()
    for repo in user_json:
        full_name = repo['full_name']
        reps.append(full_name)
    return reps

def test_type(read_repos):
    assert isinstance(read_repos, list)